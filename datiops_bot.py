#!/usr/bin/env python3

import time
import telepot
import json
import syslog
import os
import os.path
import sys
import argparse
import random
import hashlib
import datetime
import speech
import common

from stats_plot import *
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardButton, InlineKeyboardMarkup

esperaMensaje = False
to_superadmin = False
superadmin = "7404034"
aprobado = ""
moderacion = {}
frases = {}


def escribeLog(texto):
    syslog.syslog(texto)
    print(texto)
    # telegram.sendMessage(superadmin, texto, disable_notification=True)


def actualiza_frases():
    for item in reproduce.get_sentences():
        id = get_hash_texto(item)
        if id not in frases.keys():
            frases[id] = item


def teclado_usuarios(callback_str):
    botones = []
    for item in secretos["authorized_ids"]:
        if not item.startswith("-"):
            botones.append(InlineKeyboardButton(text=telegram.getChat(item)["first_name"],
                                                                callback_data=callback_str + item))
    columnas = [botones[i:i+2] for i in range(0,len(botones),2)]
    return InlineKeyboardMarkup(inline_keyboard=columnas)


def teclado_topics(callback_str=''):
    temas = reproduce.get_topics()
    temas.sort()
    botones = []
    for item in temas:
        botones.append(InlineKeyboardButton(text=item, callback_data=callback_str + item))
    columnas = [botones[i:i+2] for i in range(0,len(botones),2)]
    return InlineKeyboardMarkup(inline_keyboard=columnas)


def teclado_frases(tema):
    actualiza_frases()
    frases = reproduce.get_sentences(tema)
    botones = []
    for item in frases:
        botones.append([InlineKeyboardButton(text=item, callback_data=get_hash_texto(item))])
    return InlineKeyboardMarkup(inline_keyboard=botones)


def teclado_volumen(callback_str='volume.'):
    botones = []
    for item in range(10, 110, 10):
        botones.append(InlineKeyboardButton(text=str(item) + '%', callback_data=callback_str + str(item)))
    columnas = [botones[i:i+5] for i in range(0,len(botones),5)]
    return InlineKeyboardMarkup(inline_keyboard=columnas)


def mensaje_para_admins(mensaje, keyboard=[]):
    try:
        for admin in secretos["admin"]:
            telegram.sendMessage(admin, mensaje, reply_markup=keyboard)
    except:
        escribeLog("Error al enviar el mensaje '" + mensaje + "' a los admin")


def estado_moderacion():
    if secretos["moderation_mode"]:
        return "La moderacion de mensajes esta ACTIVADA"
    else:
        return "La moderacion de mensajes esta DESACTIVADA"


def get_hash_texto(texto):
    return hashlib.md5(texto.encode("utf-8")).hexdigest()


def get_text_from_hash(hash_texto):
    for item in moderacion.keys():
        for i in moderacion[item]:
            if i[0] == hash_texto:
                return i[1]


def hash_already_exists(chat_id, hash_texto):
    try:
        if (hash_texto, get_text_from_hash(hash_texto)) in moderacion[chat_id]:
            return True
        else:
            return False
    except KeyError:
        return False


def delete_text(chat_id, hash_texto):
    moderacion[chat_id].remove((hash_texto, get_text_from_hash(hash_texto)))


def actualiza_estadisticas(chat_id):
    global estadisticas

    week_of_year = str(datetime.datetime.utcnow().isocalendar()[1])
    year = str(datetime.datetime.utcnow().strftime("%Y"))

    if year not in estadisticas.keys():
        estadisticas[year] = {}
        estadisticas[year][week_of_year] = {}
        estadisticas[year][week_of_year][chat_id] = 1
    elif week_of_year not in estadisticas[year].keys():
        estadisticas[year][week_of_year] = {}
        estadisticas[year][week_of_year][chat_id] = 1
    elif chat_id not in estadisticas[year][week_of_year].keys():
        estadisticas[year][week_of_year][chat_id] = 1
    else:
        estadisticas[year][week_of_year][chat_id] += 1

    escribe_fichero(args["stat_file"], estadisticas)


def on_chat_message(msg):
    global esperaMensaje
    global to_superadmin

    chat_id = str(msg['chat']['id'])
    nombre_usuario = msg['from']['first_name']
    if "text" in msg.keys():
        comando = msg['text']
        voice_message_file_id = None
    elif "voice" in msg.keys():
        comando = None
        voice_message_file_id = msg['voice']['file_id']

    keyboard = InlineKeyboardMarkup(inline_keyboard=[])

    if chat_id not in secretos["authorized_ids"]:
        escribeLog("El usuario %s (%s) no esta autorizado" %(nombre_usuario, chat_id))
        mensaje = "El usuario %s (%s) quiere usar @datiops_bot, para autorizarle pulsa el boton 'AUTORIZAR'" %(nombre_usuario, chat_id)
        botones = [[InlineKeyboardButton(text='AUTORIZAR', callback_data='authorize.' + chat_id),
                InlineKeyboardButton(text='INFO usuario', callback_data='userinfo.' + chat_id)]]
        keyboard = InlineKeyboardMarkup(inline_keyboard=botones)
        mensaje_para_admins(mensaje, keyboard)
        return
    else:
        if esperaMensaje:
            esperaMensaje = False
            if comando:
                texto = comando
                hash_texto = get_hash_texto(texto)
                if secretos["moderation_mode"]:
                    if chat_id not in moderacion.keys():
                        moderacion[chat_id] = [(hash_texto, texto)]
                    elif chat_id in moderacion.keys() and not hash_already_exists(chat_id, hash_texto):
                        moderacion[chat_id].append((hash_texto, texto))
                    elif chat_id in moderacion.keys() and hash_already_exists(chat_id, hash_texto):
                        print ("El usuario %s (%s) insiste en reproducir la misma frase" %(nombre_usuario, chat_id))
                    mensaje = "%s quiere enviar el siguiente mensaje: '%s'. Permitir?" %(nombre_usuario, texto)
                    botones = [[InlineKeyboardButton(text="SÍ", callback_data="authmsgyes_" + chat_id + "_" + hash_texto), InlineKeyboardButton(text="NO", callback_data="authmsgno_" + chat_id + "_" + hash_texto)]]
                    keyboard = InlineKeyboardMarkup(inline_keyboard=botones)
                    to_superadmin = True
                else:
                    reproduce.play_message(texto)
                    actualiza_estadisticas(chat_id)
                    escribeLog("El usuario %s (%s) ha enviado el mensaje '%s'" %(nombre_usuario, chat_id, texto))
                    mensaje = "Mensaje reproducido"
            elif voice_message_file_id:
                tmp_ogg_file = "/tmp/" + voice_message_file_id + ".ogg"
                telegram.download_file(voice_message_file_id, tmp_ogg_file)
                escribeLog("El usuario %s (%s) ha enviado un mensaje de audio" %(nombre_usuario, chat_id))
                reproduce.play_mp3(tmp_ogg_file)
                actualiza_estadisticas(chat_id)
                mensaje = "Mensaje reproducido"
                os.remove(tmp_ogg_file)
        else:
            if comando == "/help":
                mensaje = """
                Estos son los comandos disponibles:
                - /text: el texto que quieras reproducir en la raspberry (después de ejecutar el comando pregunta el texto a reproducir)
                - /random: para reproducir una frase aleatoria de los temas propuestos
                - /topic: para elegir una frase en concreta de los temas propuestos
                - /mp3: para reproducir uno de los mp3 propuestos
                - /admin: solo para administradores
                """

            elif comando == "/start":
                escribeLog("El usuario %s (%s) ha iniciado chat con datiops_bot" %(nombre_usuario, chat_id))
                mensaje = 'Buenas %s!\nSoy el bot de Operaciones de DATIO. Ejecuta /help para saber los comandos que tienes disponibles. A disfrutarlos' %nombre_usuario

            elif comando == "/text" or comando == "/text@datiops_bot":
                esperaMensaje = True
                mensaje = "Por favor, dime qué quieres reproducir en la raspberry:"

            elif comando == "/random" or comando == "/random@datiops_bot":
                keyboard = teclado_topics()
                mensaje = "Por favor, elige uno de los siguientes temas:"

            elif comando == "/topic" or comando == "/topic@datiops_bot":
                keyboard = teclado_topics("¬")
                mensaje = "Por favor, elige uno de los siguientes temas:"

            elif comando == "/mp3" or comando == "/mp3@datiops_bot":
                mp3 = reproduce.get_mp3()
                mp3.sort()
                botones = []
                for item in mp3:
                    botones.append([InlineKeyboardButton(text=item, callback_data=item)])
                keyboard = InlineKeyboardMarkup(inline_keyboard=botones)
                mensaje = "Por favor, elige uno de los siguientes mp3:"

            elif comando == "/admin" or comando == "/admin@datiops_bot":
                if chat_id not in secretos["admin"]:
                    mensaje = "Lo siento pero no eres un usuario administrador. Pasa por caja para aumentar tus privilegios https://www.paypal.me/ohermosa"
                    escribeLog ("El usuario %s (%s) ha intentado ejecutar el comando '/admin'" %(nombre_usuario, chat_id))
                else:
                    mensaje = "Elige qué quieres hacer:"
                    botones = [[InlineKeyboardButton(text="Ver autorizados", callback_data="authorized"),
                            InlineKeyboardButton(text="Bannear", callback_data="ban")],
                            [InlineKeyboardButton(text="Ver administradores", callback_data="ver_admin"),
                            InlineKeyboardButton(text="Nuevo admin", callback_data="new_admin")],
                            [InlineKeyboardButton(text="Moderation STATUS", callback_data="moderation_status"),
                            InlineKeyboardButton(text="Moderation MODE", callback_data="moderation_mode")],
                            [InlineKeyboardButton(text="Enviar mensaje", callback_data="send_message"),
                            InlineKeyboardButton(text="Subir Volumen", callback_data="subir_volumen")],
                            [InlineKeyboardButton(text="KILL ME", callback_data="killme"),
                            InlineKeyboardButton(text="Estadisticas", callback_data="estadisticas")]]
                    keyboard = InlineKeyboardMarkup(inline_keyboard=botones)

            elif comando.startswith("MSG@"):
                destinatario = comando.split("@")[1]
                mensaje = comando.split("@")[2]
                chat_id = destinatario
                escribeLog("El usuario %s (%s) ha enviado como @datiops_bot el mensaje '%s' a %s" %(nombre_usuario, chat_id, mensaje, telegram.getChat(destinatario)["first_name"]))

            else:
                mensaje = "Ay %s, eres un lechón. Aprende a usar este bot ejecutando el comando /help" %nombre_usuario

            if to_superadmin:
                telegram.sendMessage(superadmin, mensaje, reply_markup=keyboard)
                to_superadmin = False
            else:
                telegram.sendMessage(chat_id, mensaje, reply_markup=keyboard)


def on_callback_query(msg):
    nombre_usuario = msg['from']['first_name']
    chat_id = str(msg['from']['id'])
    query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')

    if query_data.startswith("¬"):
        tema = query_data.split("¬")[1]
        keyboard = teclado_frases(tema)
        mensaje = "Selecciona una de las siguientes frases:"
        telegram.sendMessage(chat_id, mensaje, reply_markup=keyboard)

    if query_data in reproduce.get_topics():
        reproduce.play_random(query_data)
        actualiza_estadisticas(chat_id)
        telegram.answerCallbackQuery(query_id, text='Mensaje reproducido')
        os.popen("amixer sset PCM 93%").read()
        escribeLog("El usuario %s (%s) ha reproducido un mensaje aleatorio de %s" %(nombre_usuario, chat_id, query_data))

    elif query_data in reproduce.get_mp3():
        reproduce.play_mp3(query_data)
        if query_data == "pedo.mp3":
            reproduce.play_message("Vaya, disculpad todos, anoche cené judías")
        actualiza_estadisticas(chat_id)
        telegram.answerCallbackQuery(query_id, text='MP3 reproducido')
        escribeLog("El usuario %s (%s) ha reproducido el mp3 %s" %(nombre_usuario, chat_id, query_data))

    elif "authorize." in query_data:
        nuevo_usuario = query_data.split("authorize.")[1]
        if nuevo_usuario in secretos["authorized_ids"]:
            mensaje = "El usuario %s (%s) ya estaba autorizado" %(telegram.getChat(nuevo_usuario)["first_name"], nuevo_usuario)
        else:
            secretos["authorized_ids"].append(nuevo_usuario)
            escribe_fichero(args["config_file"], secretos)
            telegram.sendMessage(nuevo_usuario, "Has sido autorizado, ENHORABUENA!")
            escribeLog("Se ha autorizado al usuario %s a usar el bot" %nuevo_usuario)
            mensaje = "El usuario %s (%s) ha sido autorizado" %(telegram.getChat(nuevo_usuario)["first_name"], nuevo_usuario)
        telegram.answerCallbackQuery(query_id, text=mensaje)
        mensaje_para_admins(mensaje)

    elif "userinfo." in query_data:
        nuevo_usuario = query_data.split("userinfo.")[1]
        telegram.sendMessage(chat_id, json.dumps(telegram.getChat(nuevo_usuario), indent=2))

    elif query_data == "authorized":
        mensaje = "Ahora mismo hay estos usuarios autorizados:\n"
        for usuario in secretos["authorized_ids"]:
            if not usuario.startswith("-"):
                mensaje = mensaje + telegram.getChat(usuario)["first_name"] + "\n"
        telegram.answerCallbackQuery(query_id, text='Se ha mostrado la lista de usuarios autorizados')
        telegram.sendMessage(chat_id, mensaje)

    elif query_data == "killme":
        os.popen("pkill -f " + sys.argv[0])
        telegram.answerCallbackQuery(query_id, text='Me he suicidado. Volvere a la vida en 5 segundos')
        escribeLog("El usuario %s (%s) ha matado al bot" %(telegram.getChat(chat_id)["first_name"], chat_id))

    elif query_data == "ban":
        keyboard = teclado_usuarios("ban.")
        mensaje = "Elige que usuario quieres bannear:"
        telegram.sendMessage(chat_id, mensaje, reply_markup=keyboard)

    elif "ban." in query_data:
        to_ban = query_data.split("ban.")[1]
        if to_ban == superadmin or to_ban in secretos["admin"]:
            mensaje = "No se puede banear a un administrador"
        else:
            if to_ban not in secretos["authorized_ids"]:
                mensaje = "El usuario %s (%s) ya no estaba en la lista de usuarios autorizados" %(telegram.getChat(to_ban), to_ban)
            else:
                secretos["authorized_ids"].remove(to_ban)
                if to_ban in secretos["admin"]:
                    secretos["admin"].remove(to_ban)
                escribe_fichero(args["config_file"], secretos)
                telegram.sendMessage(to_ban, "Has sido baneado del bot. Para cualquier reclamacion, llama al 091")
                escribeLog ("El usuario %s (%s) ha baneado al usuario %s (%s)" %(nombre_usuario, chat_id, telegram.getChat(to_ban)["first_name"], to_ban))
                mensaje = "El usuario %s (%s) ha sido baneado" %(telegram.getChat(to_ban)["first_name"], to_ban)
        telegram.answerCallbackQuery(query_id, mensaje)
        mensaje_para_admins(mensaje)

    elif query_data == "new_admin":
        mensaje = "Elije cual de estos usuarios quieres convertirlo en administrador:"
        keyboard = teclado_usuarios("newadmin.")
        telegram.sendMessage (chat_id, mensaje, reply_markup=keyboard)

    elif query_data == "ver_admin":
        mensaje = "Ahora mismo hay estos usuarios administradores:\n"
        for usuario in secretos["admin"]:
            mensaje = mensaje + telegram.getChat(usuario)["first_name"] + "\n"
        telegram.answerCallbackQuery(query_id, text='Se ha mostrado la lista de usuarios administradores')
        telegram.sendMessage(chat_id, mensaje)

    elif "newadmin." in query_data:
        to_admin = query_data.split("newadmin.")[1]
        if to_admin in secretos["admin"]:
            mensaje = "El usuario %s (%s) ya es administrador" %(telegram.getChat(to_admin)["first_name"], to_admin)
        else:
            secretos["admin"].append(to_admin)
            escribe_fichero(args["config_file"], secretos)
            mensaje = "Se ha convertido en administrador al usuario %s (%s)" %(telegram.getChat(to_admin)["first_name"], to_admin)
            escribeLog ("El usuario %s (%s) ha convertido en admin al usuario %s (%s)" %(nombre_usuario, chat_id, telegram.getChat(to_admin)["first_name"], to_admin))
            telegram.sendMessage(to_admin,"Ya eres administrador del bot, recuerda: __'un gran poder conlleva una gran responsabilidad__'")
        telegram.answerCallbackQuery(query_id, text=mensaje)
        mensaje_para_admins(mensaje)

    elif query_data == "subir_volumen":
        mensaje = "Selecciona el volumen:"
        keyboard = teclado_volumen("volume.")
        telegram.sendMessage(chat_id, mensaje, reply_markup=keyboard)

    elif "volume." in query_data:
        new_volume = query_data.split("volume.")[1]
        reproduce.set_volume(int(new_volume))
        mensaje = "Volumen subido al " + new_volume + "%"
        telegram.answerCallbackQuery(query_id, text=mensaje)

    elif query_data == "moderation_status":
        mensaje = estado_moderacion()
        telegram.answerCallbackQuery(query_id, text=mensaje)

    elif query_data == "moderation_mode":
        secretos["moderation_mode"] = not secretos["moderation_mode"]
        escribe_fichero(args["config_file"], secretos)
        mensaje = estado_moderacion()
        telegram.answerCallbackQuery(query_id, text=mensaje)
        if chat_id != superadmin:
            telegram.sendMessage(superadmin, mensaje)
        escribeLog ("El usuario %s (%s) ha cambiado el modo moderación" %(nombre_usuario, chat_id))

    elif "authmsgyes_" in query_data:
        telegram.answerCallbackQuery(query_id, "Mensaje aceptado")
        usuario = query_data.split("_")[1]
        hash_texto = query_data.split("_")[2]
        texto = get_text_from_hash(hash_texto)
        if hash_already_exists(usuario, hash_texto):
            reproduce.play_message(texto)
            actualiza_estadisticas(chat_id)
            telegram.sendMessage(usuario, "Mensaje reproducido")
            delete_text(usuario, hash_texto)
            escribeLog("El usuario %s (%s) ha enviado el mensaje '%s' y ha sido aprobado por el superadmin" %(telegram.getChat(usuario)["first_name"], usuario, texto))
        else:
            telegram.sendMessage(superadmin, "Este mensaje ya ha sido procesado")

    elif "authmsgno_" in query_data:
        telegram.answerCallbackQuery(query_id, "Mensaje denegado")
        usuario = query_data.split("_")[1]
        hash_texto = query_data.split("_")[2]
        if hash_already_exists(usuario, hash_texto):
            telegram.sendMessage(usuario, "Dios no ha aprobado tu mensaje, anda y que te den por culo")
            delete_text(usuario, hash_texto)
            escribeLog("El usuario %s (%s) ha enviado el mensaje '%s' pero no ha sido aprobado por el superadmin" %(telegram.getChat(usuario)["first_name"], usuario, get_text_from_hash(hash_texto)))
        else:
            telegram.sendMessage(superadmin, "Este mensaje ya ha sido procesado")

    elif query_data == "send_message":
        keyboard = teclado_usuarios("message.")
        mensaje = "Elige que usuario al que quieres enviar un mensaje:"
        telegram.sendMessage(chat_id, mensaje, reply_markup=keyboard)

    elif "message." in query_data:
        to_message = query_data.split("message.")[1]
        mensaje = "Envia 'MSG@" + to_message + "@tu mensaje' para enviarle un mensaje a " + telegram.getChat(to_message)["first_name"]
        telegram.sendMessage(chat_id, mensaje)

    elif query_data in frases.keys():
        reproduce.play_message(frases[query_data])
        actualiza_estadisticas(chat_id)
        escribeLog("El usuario %s (%s) ha reproducido la frase '%s'" %(nombre_usuario, chat_id, frases[query_data]))
        mensaje = "Mensaje reproducido"
        telegram.answerCallbackQuery(query_id, text=mensaje)

    elif query_data == "estadisticas":
        this_year = str(datetime.datetime.utcnow().strftime("%Y"))
        this_week = str(datetime.datetime.utcnow().isocalendar()[1])
        if unsupported in estadisticas[this_year][this_week].keys():
            del(estadisticas[this_year][this_week][unsupported])
        grafico_img = create_graph([telegram.getChat(x)['first_name'] for x in list(estadisticas[this_year][this_week].keys())], list(estadisticas[this_year][this_week].values()), title="week #" + this_week + " statistics")
        telegram.sendPhoto(chat_id, open(grafico_img, 'rb'))
        delete_graph__file(grafico_img)
        telegram.answerCallbackQuery(query_id, text="Generado grafico de estadisticas")



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config_file", required=True, help="Define el fichero json de configuracion del script")
    parser.add_argument("-s", "--stat_file", required=True, help="Define el fichero de estadisticas")
    args = parser.parse_args()
    args = vars(args)
    if os.path.islink(os.path.abspath(sys.argv[0])):
        mydir = os.path.dirname(os.readlink(sys.argv[0]))
    else:
        mydir = os.path.dirname(os.path.abspath(sys.argv[0]))
    reproduce = speech.ispeak(mydir)
    actualiza_frases()
    #print(frases)

    if os.path.isfile(args["config_file"]) and os.path.isfile(args["stat_file"]):
        secretos = lee_fichero(args["config_file"])
        estadisticas = lee_fichero(args["stat_file"])
        if superadmin not in secretos["admin"]:
            secretos["admin"].append(superadmin)
            escribe_fichero(args["config_file"], secretos)
        # print (secretos)
        telegram = telepot.Bot(secretos["token"])
        MessageLoop(telegram, {'chat': on_chat_message, 'callback_query': on_callback_query}).run_as_thread()
        while 1:
            secretos = lee_fichero(args["config_file"])
            time.sleep(10)
    else:
        print("Alguno de los ficheros '%s' o '%s' no existe" %(args["config_file"], args["stat_file"]))
        sys.exit(1)
