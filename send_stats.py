#!/usr/bin/env python3

import telepot
import json
import os
import os.path
import sys
import argparse
import random
import datetime
import speech
import common

from stats_plot import *



def envia_stats():
    this_year = str(datetime.datetime.utcnow().strftime("%Y"))
    this_week = str(datetime.datetime.utcnow().isocalendar()[1])
    if this_week in estadisticas[this_year].keys():
        week_stats = estadisticas[this_year][this_week]
        if unsupported in week_stats.keys():
            del(week_stats[unsupported])
        minuse = 1000
        for item in week_stats.items():
            if int(item[1]) < minuse:
                minuse = int(item[1])
                username_id = item[0]
                username = telegram.getChat(item[0])["first_name"]
        grafico_img = create_graph([telegram.getChat(x)['first_name'] for x in list(week_stats.keys())], list(week_stats.values()), title="El puto perdedor ha sido " + username)
        telegram.sendPhoto(unsupported, open(grafico_img, 'rb'))
        telegram.sendMessage(unsupported, "Lo siento @" + telegram.getChat(username_id)['username'] + ", eres el que menos tiempo ha perdido conmigo, te toca traer " + random.choice(["porras", "churros", "tortillas", "empanadas", "donuts"]) + " el lunes")
        delete_graph__file(grafico_img)
    else:
        telegram.sendMessage(unsupported, "Estoy triste, ni dios me ha usado esta semana")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config_file", required=True, help="Define el fichero json de configuracion del script")
    parser.add_argument("-s", "--stat_file", required=True, help="Define el fichero de estadisticas")
    args = parser.parse_args()
    args = vars(args)

    reproduce = speech.ispeak()

    if os.path.isfile(args["config_file"]) and os.path.isfile(args["stat_file"]):
        secretos = lee_fichero(args["config_file"])
        estadisticas = lee_fichero(args["stat_file"])

        telegram = telepot.Bot(secretos["token"])
        envia_stats()

    else:
        print("Alguno de los ficheros '%s' o '%s' no existe" %(args["config_file"], args["stat_file"]))
        sys.exit(1)
