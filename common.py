#!/usr/bin/env python3

import json

secretos = {}
estadisticas = {}
unsupported = "-1001293493454"


def lee_fichero(configfile):
    with open(configfile, 'r') as f:
        return json.load(f)


def escribe_fichero(configfile, data):
    with open(configfile, 'w') as f:
        json.dump(data, f)
